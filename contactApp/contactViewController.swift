//
//  contactViewController.swift
//  contactApp
//
//  Created by Victoria Alvizurez on 10/12/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class contactViewController: UIViewController,  MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    @IBOutlet var emailButotn: UIButton!
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendText(_ sender: Any) {
        
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Message Body"
            controller.recipients = ["%@", phoneNum.text]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func calluser(_ sender: Any) {
        
        if let url = URL(string: "tel://\"(phoneNum.text)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            }
            else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func emailButton(_ sender: UIButton) {
        
        if(MFMailComposeViewController.canSendMail()){
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients(["%@", email.text])
            composeVC.setSubject("Contact")
            composeVC.setMessageBody("Hello this is my message!", isHTML: false)
          
            self.present(composeVC, animated:true, completion: nil)
            
        }
        
        func MailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
            // Dismiss the mail compose view controller.
            controller.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
  
    var contactName = ""
    var passedValue = ""
    let realm = try! Realm()
    
    @IBOutlet var name: UITextView!
    @IBOutlet var phoneNum: UITextView!
    @IBOutlet var address: UITextView!
    
    @IBOutlet var email: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
       contactName = passedValue
      getContact()
     
   
    }

    func getContact(){
        var contact: Results<contacts> = { self.realm.objects(contacts.self).filter("name == %@", contactName) }()
     
        for person in contact{
            name.text = person.name
            phoneNum.text = person.phoneNum
            email.text   = person.email
            address.text = person.address
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
