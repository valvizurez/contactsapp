//
//  ViewController.swift
//  contactApp
//
//  Created by Victoria Alvizurez on 10/10/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let realm = try! Realm()
    let contact = try! Realm().objects(contacts.self)
   @IBOutlet var tableView: UITableView!
      var valueToPass = ""
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return contact.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath) as UITableViewCell
        let contacts = self.contact[indexPath.row]
        cell.textLabel?.text = contacts.name
        return cell
        
    }
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow;
        let currentCell = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
        valueToPass = (currentCell?.textLabel?.text)!
        self.performSegue(withIdentifier: "moveContact", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "moveContact") {
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! contactViewController
            // your new view controller should have property that will store passed value
            viewController.passedValue = valueToPass
        }
    }
        
 
    override func viewWillAppear(_ animated: Bool) { tableView.reloadData() }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

