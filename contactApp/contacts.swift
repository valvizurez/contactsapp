//
//  contacts.swift
//  contactApp
//
//  Created by Victoria Alvizurez on 10/12/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import Foundation
import RealmSwift

class contacts : Object {
    dynamic var name = ""
    dynamic var phoneNum = ""
    dynamic var address = ""
    dynamic var email = ""
}
