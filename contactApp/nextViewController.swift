//
//  nextViewController.swift
//  contactApp
//
//  Created by Victoria Alvizurez on 10/12/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import RealmSwift

class nextViewController: UIViewController {
    
    let realm = try! Realm()
    let contact = try! Realm().objects(contacts.self)
   
    @IBOutlet var name: UITextField!
    @IBOutlet var phoneNum: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var address: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saveBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(buttonTapped(_:)))
        
        let deleteBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(buttonTapped(_:)))
        
        self.navigationItem.rightBarButtonItems = [saveBtn]
        self.navigationItem.leftBarButtonItems = [deleteBtn]
        
    
    }
    
    @objc func buttonTapped(_ sender : UIButton)
    { try! realm.write() {
        let newContact = contacts()
           newContact.name = name.text!
            newContact.phoneNum = phoneNum.text!
            newContact.address = address.text!
        newContact.email = email.text!
            self.realm.add(newContact)
        print(newContact)
        let _ = navigationController?.popViewController(animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
